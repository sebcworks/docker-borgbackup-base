Very basic Borg Container
=========================

Very basic Borg backup tool container, used to only have a base container to use for client and server container.

It install Borg and some important dependencies (like openssh)

It may also be used to create volumes for client and/or server containers.

Related
-------

* Example of a borg client image that makes use of this "base": [https://hub.docker.com/r/sebcworks/borgbackup-client/]
* Example of a borg server image: [https://hub.docker.com/r/sebcworks/borgbackup-server/]
